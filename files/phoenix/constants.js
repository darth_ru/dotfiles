const MOD = ['ctrl', 'alt', 'cmd'];
const MOD_SHIFT = [...MOD, 'shift'];

const DOUBLE_KEY_INTERVAL = 250;

const PADDING = 0.005;
